#!/usr/bin/env python

"""Main program to create an index for information retrieval"""

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import os
import sys
import time
import csv
import argparse
import struct
import matplotlib.pyplot as plt
from math import log2

from tqdm import tqdm 

from utilities.file_lister import file_lister
from utilities.file_to_string import file_to_string
from utilities.create_dirs import create_dirs

from tokenizer import tokenizer

RES_DIR = "results"
INDEX_DIR = "index"
VOC_DIR = os.path.join(INDEX_DIR, "vocabulary")
COL_FILE = os.path.join(INDEX_DIR, "collection.csv")
VOC_FILE = os.path.join(INDEX_DIR, "vocabulary.csv")
POSTING_FILE = os.path.join(INDEX_DIR, "posting_lists.pl")

TERM_KEY = "term"
DOC_ID_KEY = "doc_id"
FREQ_KEY = "freq"
DF_KEY = "DF"
POSTING_KEY = "posting_list"
IDF_KEY = "IDF"
OFFSET_KEY = "offset"
SEP = "\t"

POSTING_SIZE = 8 # bytes
TOP_K = 10

_fragment_count = 0
_document_overhead = {}
_total_overhead = 0

_compression = None
_dgaps = False
_compression_time = 0
_decompression_time = 0

AND_OP = "y"
OR_OP = "o"
NOT_OP = "-"



def index_main(dirname, encoding="UTF-8", stopword_filename=None):
	global _fragment_count, _compression_time, _compression, _dgaps
	collection = {}
	vocabulary = {}
	
	collection_size = 0
	document_size_list = []
	
	create_dirs([VOC_DIR, RES_DIR])
	with open(POSTING_FILE, "w"):
		pass

	vocabulary_fragment = {}
	stopwords = tokenizer.get_stopwords(stopword_filename)
	doc_path_list = file_lister(dirname)
	n_docs = len(doc_path_list)
	doc_id = 0

	print("Indexing", n_docs, "documents...")
	with tqdm(total = n_docs) as pbar:
		while doc_id < n_docs:
			doc_name = doc_path_list[doc_id]
			doc = file_to_string(doc_name, encoding)
			collection_size += len(doc)
			document_size_list.append(len(doc))
			collection[doc_id] = doc_name

			token_list = tokenizer.tokenizar(doc)
			term_list = tokenizer.sacar_palabras_vacias(token_list, stopwords)

			for term, freq in term_list.items():
				if term in vocabulary_fragment:
					vocabulary_fragment[term][DF_KEY] += 1
					vocabulary_fragment[term][POSTING_KEY][doc_id] = freq
				# new term found
				else:
					vocabulary_fragment[term] = {
						DF_KEY: 1,
						POSTING_KEY: {doc_id: freq}
					}

			doc_id += 1
			pbar.update()

			if doc_id % 1000 == 0:
				vocabulary_fragment = {k:vocabulary_fragment[k] for k in sorted(vocabulary_fragment)}
				save_vocabulary_fragment(vocabulary_fragment)
				vocabulary_fragment = {}


	if vocabulary_fragment:
		vocabulary_fragment = {k:vocabulary_fragment[k] for k in sorted(vocabulary_fragment)}
		save_vocabulary_fragment(vocabulary_fragment)

	print("Merging", _fragment_count, "vocabulary fragments...")
	vocabulary = merge_and_index()
	print("Compression time:", _compression_time)

	posting_list_sizes = []
	collection_len = len(collection)
	for term in vocabulary:
		df = vocabulary[term][DF_KEY]
		vocabulary[term][IDF_KEY] = log2(collection_len / df)
		posting_list_sizes.append(df * POSTING_SIZE)

	print("Saving index to drive...")
	save_vocabulary(vocabulary)
	save_collection(collection)

	print("Index overhead: {:.2f}%".format(_total_overhead / collection_size * 100))
	for doc_id, doc_oh in _document_overhead.items():
		print("Document {} overhead: {:.2f}%".format(doc_id, doc_oh / document_size_list[int(doc_id)] * 100))

	print("Plotting posting list distribution...")
	plt.hist(posting_list_sizes, 20)
	plt.title('Posting list size distribution')
	plt.xlabel('Size')
	plt.ylabel('Frequency')
	plt.savefig(os.path.join(RES_DIR, "dist_posting_lists.png"))
	
	print("Done!")


def merge_and_index(voc_dir = VOC_DIR, delimiter = SEP):
	global _document_overhead, _total_overhead, _compression, _dgaps
	
	voc_filename_list = file_lister(voc_dir)
	voc_file_list = [open(voc_filename, "r") for voc_filename in voc_filename_list]
	voc_fragment_list = [csv.DictReader(voc_file, delimiter = delimiter) for voc_file in voc_file_list]
	rows = []
	offset = 0 # Current offset

	# Advance pointer to first row of file
	for voc_fragment in voc_fragment_list:
		rows.append(next(voc_fragment))
	
	vocabulary = {}
	posting_list = {}
	current_term = ""
	term_finished = False
	
	while rows:
		# Save last term to know when to save a term to drive
		last_term = current_term
		# Save which file should advance its pointer
		current_voc_fragment = 0
		# Initialize with first term
		current_term = rows[current_voc_fragment][TERM_KEY]
		current_doc_id = rows[current_voc_fragment][DOC_ID_KEY]

		# Check first row of every file or vocabulary fragment
		voc_fragment_index = 1
		while voc_fragment_index < len(rows):
			row = rows[voc_fragment_index]
			
			if current_term > row[TERM_KEY]:
				current_term = row[TERM_KEY]
				current_doc_id = row[DOC_ID_KEY]
				current_voc_fragment = voc_fragment_index
			
			elif current_term == row[TERM_KEY]:
				if current_doc_id > row[DOC_ID_KEY]:
					current_doc_id = row[DOC_ID_KEY]
					current_voc_fragment = voc_fragment_index

			voc_fragment_index += 1
	
		freq = rows[current_voc_fragment][FREQ_KEY]

		if last_term == current_term:
			term_finished = False
		
		# Save completed posting list to drive if term is finished
		if term_finished:
			# Add current term to vocabulary
			vocabulary[last_term] = {
				DF_KEY: len(posting_list),
				OFFSET_KEY: offset
			}
			# Save posting list to drive
			# New offset is last offset + bytes saved
			if last_term == "indios":
				print(posting_list)
			offset += save_posting_list(posting_list)
			# Prepare new posting list
			posting_list = {current_doc_id: freq}

		# Term still hasn't completed its posting list
		else:
			# Append to current posting list
			posting_list[current_doc_id] = freq

		# Document overhead
		if current_doc_id in _document_overhead:
			_document_overhead[current_doc_id] += POSTING_SIZE
		else:
			_document_overhead[current_doc_id] = POSTING_SIZE

		# Total overhead
		_total_overhead += POSTING_SIZE

		term_finished = True
		
		# Advance pointer
		try:
			rows[current_voc_fragment] = next(voc_fragment_list[current_voc_fragment])
		# File is finished. Remove from list of files to process
		# (Actually, just delete its row entry i.e. it has no more rows to process)
		except StopIteration:
			del rows[current_voc_fragment]


	for voc_file in voc_file_list:
		voc_file.close()

	for term in vocabulary:
		_total_overhead += len(term) + 8 # DF + offset

	return vocabulary



def save_vocabulary_fragment(voc_fragment, FILENAME = "voc_fragment", delimiter = SEP):
	global _fragment_count
	filename = os.path.join(VOC_DIR, FILENAME + str(_fragment_count) + ".txt")
	_fragment_count += 1
	with open(os.path.join(filename), "w") as file:
		file.write(TERM_KEY + delimiter + DOC_ID_KEY + delimiter + FREQ_KEY + "\n")
		for term, data in voc_fragment.items():
			for doc_id, freq in data[POSTING_KEY].items():
				file.write(term + delimiter + str(doc_id) + delimiter + str(freq) + "\n")
	return filename



def save_vocabulary(vocabulary, filename=VOC_FILE, delimiter=SEP):
	with open(filename, "w") as f:
		f.write(TERM_KEY + delimiter + DF_KEY + delimiter + IDF_KEY + delimiter + OFFSET_KEY + "\n")
		for term in vocabulary:
			df = str(vocabulary[term][DF_KEY])
			idf = str(vocabulary[term][IDF_KEY])
			offset = str(vocabulary[term][OFFSET_KEY])
			f.write(term + delimiter + df + delimiter + idf + delimiter + offset + "\n")



def read_vocabulary(filename=VOC_FILE, delimiter=SEP):
	vocabulary = {}
	with open(filename, "r") as f:
		next(f)
		for line in f:
			line_split = line.split(delimiter)
			vocabulary[line_split[0]] = {
				DF_KEY: int(line_split[1]),
				IDF_KEY: float(line_split[2]),
				OFFSET_KEY: int(line_split[3])
			}
	return vocabulary



def save_collection(collection, filename=COL_FILE, delimiter=SEP):
	with open(filename, "w") as f:
		f.write(DOC_ID_KEY + delimiter + "doc_name\n")
		for doc_id, doc_name in collection.items():
			f.write(str(doc_id) + delimiter + doc_name + "\n")



def read_collection(filename=COL_FILE, delimiter=SEP):
	collection = {}
	with open(filename, "r") as f:
		next(f)
		for line in f:
			line_split = line.split(delimiter)
			collection[line_split[0]] = line_split[1].strip()
	return collection



def save_posting_list(posting_list, filename=POSTING_FILE):
	"""Saves the posting list to a file.
	Returns the amount of bytes saved.

	Keyword arguments:
	posting_list -- the posting list of the term
	filename -- name of the file where the posting_lists will be saved
	compression -- compression algorithm to use. elias-gamma, variable length encoding (vle) or None
	"""
	global _compression, _dgaps
	posting_list_write = []
	if _dgaps:
		last_doc_id = 0
		for doc_id, freq in posting_list.items():
			posting_list_write.extend([int(doc_id) - last_doc_id, int(freq)])
			last_doc_id = int(doc_id)
	else:	
		for doc_id, freq in posting_list.items():
			posting_list_write.extend([int(doc_id), int(freq)])
	
	strc = None
	size = 0
	binary_string = ""
	if _compression == "elias-gamma":
		binary_string = encode_elias_gamma(posting_list_write)

	elif _compression == "vle":
		binary_string = encode_vle(posting_list_write)

	else: 
		strc = struct.pack("I" * len(posting_list_write), *posting_list_write)
		size = POSTING_SIZE * len(posting_list)
	
	if binary_string:
		bytes_to_write = []
		for i in range(0, len(binary_string), 8):
			substring = binary_string[i:i+8]
			while len(substring) < 8:
				substring += "0"
			byte = int(substring, 2)
			bytes_to_write.append(byte)
		size = len(bytes_to_write)
		strc = struct.pack("B" * size, *bytes_to_write)

	with open(filename, "ab") as f:
		f.write(strc)
	
	return size



def read_posting_list(term, vocabulary, filename=POSTING_FILE):
	global _compression, _dgaps
	if POSTING_KEY in vocabulary[term]:
		return vocabulary[term][POSTING_KEY]

	offset = int(vocabulary[term][OFFSET_KEY])
	df = int(vocabulary[term][DF_KEY])
	posting_list_read = []
	with open(filename, "rb") as f:
		f.seek(offset)
		content = f.read(df * POSTING_SIZE)
		if _compression:
			strc = struct.unpack("B" * len(content), content)
			binary_string = ""
			for byte in strc:
				substring = bin(byte)[2:]
				while len(substring) < 8:
					substring = "0" + substring
				binary_string += substring
			if _compression == "elias-gamma":
				posting_list_read = decode_elias_gamma(binary_string)[:df*2]
			elif _compression == "vle":
				posting_list_read = decode_vle(binary_string)[:df*2]
		else:
			posting_list_read = struct.unpack("I" * df * 2, content)

	i = 0
	posting_list = {}
	if _dgaps:
		last_doc_id = 0
		while i < len(posting_list_read):
			doc_id = posting_list_read[i] + last_doc_id
			last_doc_id = doc_id
			posting_list[doc_id] = posting_list_read[i+1]
			i += 2
	else:
		while i < len(posting_list_read):
			posting_list[posting_list_read[i]] = posting_list_read[i+1]
			i += 2

	vocabulary[term][POSTING_KEY] = posting_list
	return posting_list



######################################################################################################################
# Compression methods																								##
######################################################################################################################

def encode_elias_gamma(integer_list):
	global _compression_time
	t = time.time()
	elias_string = ""
	for integer in integer_list:
		integer = integer + 1
		if integer == 1:
			elias_string += "1"
		else:
			zeroes = int(log2(integer))
			elias_string += "0" * zeroes
			elias_string += bin(integer)[2:]
	_compression_time += time.time() - t
	return elias_string 

def decode_elias_gamma(elias_string):
	global _decompression_time
	t = time.time()
	i = 0
	integer_list = []
	size = len(elias_string)
	while i < size:
		zeroes = 0
		binary_string = ""
		while i < size and elias_string[i] == "0":
			zeroes += 1
			i += 1
		while i < size and zeroes:
			binary_string += elias_string[i]
			zeroes -= 1
			i += 1
		if i < size:
			binary_string += elias_string[i]
			integer_list.append(int(binary_string, 2)-1)
			i += 1
	_decompression_time += time.time() - t
	return integer_list



def encode_vle(integer_list):
	global _compression_time
	t = time.time()
	vle_string = ""
	for integer in integer_list:
		binary_string = bin(integer)[2:]
		
		# Process backwards
		blocks = []
		i = len(binary_string)
		while i > 0:
			blocks.append(binary_string[max(i-7, 0):i])
			i -= 7
		# Add padding
		first_block = blocks[-1]
		while len(first_block) < 7:
			first_block = "0" + first_block
		blocks[-1] = first_block

		i = len(blocks) - 1
		while i > 0:
			vle_string += "1" + blocks[i]
			i -= 1
		vle_string += "0" + blocks[i]

	_compression_time += time.time() - t
	return vle_string

def decode_vle(vle_string):
	global _decompression_time
	t = time.time()

	integer_list = []
	i = 0
	binary_string = ""
	while i < len(vle_string):
		block = vle_string[i:i+8]
		binary_string += block[1:]
		if block[0] == "0":
			integer_list.append(int(binary_string, 2))
			binary_string = ""
		
		i += 8

	_decompression_time += time.time() - t
	return integer_list



######################################################################################################################
# Retrieval model																									##
######################################################################################################################

def query_main(model="boolean", stopword_filename=None):
	global _decompression_time
	
	stopwords = tokenizer.get_stopwords(stopword_filename)
	collection = read_collection()
	vocabulary = read_vocabulary()
	while True:
		print("Enter Query:")
		query = input()
		
		if model == "positional":
			pass

		# boolean query operators
		if model == "boolean":
			query_operators = []
			word_list = query.split()
			i = 1
			while i < len(word_list):
				query_operators.append(word_list[i])
				i += 2

		query_token_list = tokenizer.tokenizar(query)
		query_term_list = tokenizer.sacar_palabras_vacias(query_token_list, stopwords)
		non_terms = {}
		for term in query_term_list:
			# Only consider terms present in the collection
			if term not in vocabulary:
				non_terms[term] = 1
		# Take out non present terms
		for term in non_terms:
			query_term_list.pop(term)

		ranking = []
		if model == "vector":
			ranking = vectorial_resolve_query(query_term_list, vocabulary)
		elif model == "positional":
			pass
		else:
			t = time.time()
			ranking = boolean_resolve_query(query_term_list, query_operators, vocabulary)
			print("Time with index in drive:", str(time.time() - t))
			t = time.time()
			ranking = boolean_resolve_query(query_term_list, query_operators, vocabulary)
			print("Time with index in memory:", str(time.time() - t))

		if _compression is not None:
			print("Decompression time:", _decompression_time)
		
		print("Ranking:")
		for rank, doc in enumerate(ranking[:TOP_K]):
			print(str(rank+1) + ":", collection[str(doc)])



def boolean_resolve_query(query_term_list, query_operators, vocabulary):
	query_terms = list(query_term_list.keys())
	result = set()
	term = query_terms[0]
	posting_list = read_posting_list(term, vocabulary)
	print(posting_list)

	for doc_id in posting_list:
		result.add(int(doc_id))

	for i in range(1, len(query_terms)):
		docs = set()
		term = query_terms[i]
		posting_list = read_posting_list(term, vocabulary)
		for doc_id in posting_list:
			docs.add(int(doc_id))

		op = query_operators[i-1]
		if op == AND_OP:
			result = result.intersection(docs)
		elif op == NOT_OP:
			result = result.difference(docs)
		else:
			result = result.union(docs)

	return list(result)
			
			

def vectorial_resolve_query(query_term_list, vocabulary):
	query_vector = get_query_vector(query_term_list, vocabulary)
	document_vector_list = get_document_vector_list(query_term_list, vocabulary)
	document_similarities = {}
	for doc, doc_vector in document_vector_list.items():
		document_similarities[doc] = cosine_similarity(doc_vector, query_vector)
	return sorted(document_similarities, key=document_similarities.get, reverse=True)



def get_query_vector(query_term_list, vocabulary):
	query_vector = {}
	max_freq = 1
	# Get frequencies
	for term in query_term_list:
		if term not in query_vector:
			query_vector[term] = 1
		else:
			query_vector[term] += 1
			if query_vector[term] > max_freq:
				max_freq = query_vector[term]

	# Weigh terms
	for term in query_vector:
		# Wq = 0.5 + 0.5 + TF / max(TF) * log (N / ni)
		query_vector[term] = (0.5 + 0.5 * (query_vector[term] / max_freq)) * vocabulary[term][IDF_KEY]

	return query_vector



def get_document_vector_list(query_term_list, vocabulary):
	documents_vector_list = {}

	for term in query_term_list:
		posting_list = read_posting_list(term, vocabulary) 
		for doc_id in posting_list:
			# Save weight as TF * IDF
			term_weight = posting_list[doc_id] * vocabulary[term][IDF_KEY]
			if doc_id not in documents_vector_list:
				documents_vector_list[doc_id] = {term: term_weight}
			else:
				documents_vector_list[doc_id][term] = term_weight

	return documents_vector_list



def cosine_similarity(doc_vector, query_vector):
	#  Angle between vectors = D . Q / ||D|| * ||Q||
	numerator = 0
	doc_norm = 0
	query_norm = 0
	for term in query_vector:
		if term in doc_vector:
			numerator += doc_vector[term] * query_vector[term]
			doc_norm += doc_vector[term] ** 2
		query_norm += query_vector[term] ** 2
	
	if doc_norm == 0 or query_norm == 0: return 0
	return numerator / ((doc_norm ** (1/2)) * (query_norm ** (1/2)))


################################################## MAIN ####################################################


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Create index for information retrieval.")
	parser.add_argument("-i", "--index", help="Directory with documents to index")
	parser.add_argument("-e", "--encoding", help="Encoding to use (if not specified, use UTF-8)", default="UTF-8")
	parser.add_argument("-s", "--stopwords", help="Stopwords file to use")
	parser.add_argument("-q", "--query", help="Query a previously created index", action="store_true")
	parser.add_argument("-m", "--model", help="Model to use (boolean, vector, positional)", default="boolean")
	parser.add_argument("-c", "--compression", help="Use a compression algorithm (elias-gamma or vle)")
	parser.add_argument("-d", "--dgaps", help="Store dgaps instead of doc_ids", action="store_true")
	args = parser.parse_args()
	_compression = args.compression
	_dgaps = args.dgaps
	if args.index:
		index_main(args.index, args.encoding, args.stopwords)
	if args.query:
		query_main(args.model, args.stopwords)