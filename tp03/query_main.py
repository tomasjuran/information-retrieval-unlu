#!/usr/bin/env python

"""Main program to resolve queries using a vector model"""

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import sys
import argparse
from math import log2

from tqdm import tqdm 

from utilities.file_lister import file_lister
from utilities.file_to_string import file_to_string
from utilities.create_dirs import create_dirs

from tokenizer import tokenizer

TOP_K = 10
collection = {}
vocabulary = {}

def main(dirname, encoding="UTF-8", stopword_filename=None):
	global collection, vocabulary
	stopwords = tokenizer.get_stopwords(stopword_filename)
	collection, vocabulary = index(dirname, encoding, stopwords)
	while True:
		print("Enter Query:")
		query = input()
		ranking = resolve_query(query, stopwords)
		print("Ranking:")
		for rank, doc in enumerate(ranking[:TOP_K]):
			print(str(rank+1) + ":", collection[doc])



def index(dirname, encoding, stopwords):
	collection = {}
	vocabulary = {}

	doc_path_list = file_lister(dirname)
	n_docs = len(doc_path_list)
	doc_id = 0
	print("Indexing", n_docs, "documents...")
	with tqdm(total = n_docs) as pbar:
		while doc_id < n_docs:
			doc_name = doc_path_list[doc_id]
			doc = file_to_string(doc_name, encoding)
			collection[doc_id] = doc_name

			token_list = tokenizer.tokenizar(doc)
			term_list = tokenizer.sacar_palabras_vacias(token_list, stopwords)

			for term, freq in term_list.items():
				if term in vocabulary:
					vocabulary[term]["DF"] += 1
					vocabulary[term]["posting_list"][doc_id] = freq
				# new term found
				else:
					vocabulary[term] = {
						"DF": 1,
						"posting_list": {doc_id: freq}
					}

			doc_id += 1
			pbar.update()

	# IDF
	collection_size = len(collection)
	for term in vocabulary:
		vocabulary[term]["IDF"] = log2(collection_size / vocabulary[term]["DF"])

	return collection, vocabulary



def resolve_query(query, stopwords):
	query_token_list = tokenizer.tokenizar(query)
	query_term_list = tokenizer.sacar_palabras_vacias(query_token_list, stopwords)
	query_vector = get_query_vector(query_term_list)
	document_vector_list = get_document_vector_list(query_term_list)
	document_similarities = {}
	for doc, doc_vector in document_vector_list.items():
		document_similarities[doc] = cosine_similarity(doc_vector, query_vector)
	return sorted(document_similarities, key=document_similarities.get, reverse=True)



def get_query_vector(query_term_list):
	global vocabulary
	query_vector = {}
	non_terms = {}
	max_freq = 1
	# Get frequencies
	for term in query_term_list:
		# Only consider terms present in the collection
		if term not in vocabulary:
			non_terms[term] = 1
		else:
			if term not in query_vector:
				query_vector[term] = 1
			else:
				query_vector[term] += 1
				if query_vector[term] > max_freq:
					max_freq = query_vector[term]

	# Take out non present terms
	for term in non_terms:
		query_term_list.pop(term)

	# Weigh terms
	for term in query_vector:
		# Wq = 0.5 + 0.5 + TF / max(TF) * log (N / ni)
		query_vector[term] = (0.5 + 0.5 * (query_vector[term] / max_freq)) * vocabulary[term]["IDF"]

	return query_vector



def get_document_vector_list(query_term_list):
	global vocabulary
	documents_vector_list = {}

	for term in query_term_list:
		for doc_id in vocabulary[term]["posting_list"]:
			# Save weight as TF * IDF
			term_weight = vocabulary[term]["posting_list"][doc_id] * vocabulary[term]["IDF"]
			if doc_id not in documents_vector_list:
				documents_vector_list[doc_id] = {term: term_weight}
			else:
				documents_vector_list[doc_id][term] = term_weight

	return documents_vector_list



def cosine_similarity(doc_vector, query_vector):
	#  Angle between vectors = D . Q / ||D|| * ||Q||
	numerator = 0
	doc_norm = 0
	query_norm = 0
	for term in query_vector:
		if term in doc_vector:
			numerator += doc_vector[term] * query_vector[term]
			doc_norm += doc_vector[term] ** 2
		query_norm += query_vector[term] ** 2
	
	if doc_norm == 0 or query_norm == 0: return 0
	return numerator / ((doc_norm ** (1/2)) * (query_norm ** (1/2)))



if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Query a collection using a vector model.")
	parser.add_argument("dirname", help="Directory with documents to process")
	parser.add_argument("-e", "--encoding", help="Encoding to use (if not specified, use UTF-8)")
	parser.add_argument("-s", "--stopwords", help="Stopwords file to use")
	args = parser.parse_args()
	main(args.dirname, args.encoding, args.stopwords)