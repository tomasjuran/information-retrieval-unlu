#!/usr/bin/env python

"""Simple HTTP Crawler made with Python"""

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import re
import time
import os, os.path
import argparse
import subprocess
import bisect
import requests
from requests.exceptions import Timeout

from bs4 import BeautifulSoup
import networkx as nx
import pygraphviz
import matplotlib.pyplot as plt
from tqdm import tqdm

MAX_CRAWL_DEFAULT = 50
MAX_DEPTH_DEFAULT = 4

TIMEOUT = 2 # Timeout for expiring requests in seconds
WAIT = 0.05 # Time between requests in seconds

RES_DIR = "results"

def handle_url(URL):
	""" Get document contents given an URL via HTTP.
	Returns a tuple (URL, content).
	URL returned may be different if agent was redirected.
	"""
	time.sleep(WAIT)
	r = requests.get(URL, timeout=TIMEOUT, stream=True)
	URL = r.url
	if not r.headers["Content-Type"].startswith("text/html"):
		print("Not html:", URL)
		return (URL, BeautifulSoup("", features="html.parser"))

	return (URL, BeautifulSoup(r.text, features="html.parser"))



def pass_filter(URL):
	return URL.startswith("http://") or URL.startswith("https://")



def main(seed, max_crawl = MAX_CRAWL_DEFAULT, max_depth = MAX_DEPTH_DEFAULT, stats = False):
	todo_list = {}
	seed_count = 0
	with open(seed) as file:
		for URL in file:
			todo_list[URL.strip()] = 0
			seed_count += 1
	done_list = {}
	error_list = {}
	print("Crawling " + str(max_crawl) + " documents...")
	pbar = tqdm(total = max_crawl, initial = 1)
	crawled = 1
	while todo_list and crawled < max_crawl:
		URL = list(todo_list.keys())[0]
		depth = todo_list.pop(URL)
		if (depth >= max_depth):
			continue
		curr_depth = depth + 1

		try:
			# Links that redirect to the same page are considered the same
			pre_redir_URL = URL
			(URL, content) = handle_url(URL)
			
			done_list_len = len(done_list)
			if URL not in done_list:
				done_list[URL] = {"id": done_list_len, "outlinks": {}}
				pre_redir_id = done_list_len
			else:
				pre_redir_id = done_list[URL]["id"]

			if pre_redir_URL not in done_list:
				done_list[pre_redir_URL] = {"id": pre_redir_id, "true_url": URL}
			
			for anchor in content.select("a"):
				if not anchor.has_attr("href"):
					continue
				
				link = anchor["href"]
				if link not in error_list and pass_filter(link):
					if link not in done_list and link not in todo_list:
						todo_list[link] = curr_depth
						crawled += 1
						pbar.update()
					
					if link in done_list[URL]["outlinks"]:
						done_list[URL]["outlinks"][link] += 1
					else:
						done_list[URL]["outlinks"][link] = 1
				
				if crawled >= max_crawl:
					break
					
		except Timeout:
			print("Timeout on URL:", URL)
			error_list[URL] = 1
		except Exception as error:
			print("Error processing URL:", URL)
			print(error)
			error_list[URL] = 1

	pbar.close()
	if (len(todo_list) + len(done_list)) < max_crawl:
		print("Could not crawl", max_crawl, "documents. Try adjusting depth or increasing seed size")
	print("Crawling finished! Creating graph...")
	graph = create_graph(done_list)

	if not os.path.isdir(RES_DIR):
		os.mkdir(RES_DIR)

	if stats:
		compare_stats(graph, seed_count, RES_DIR)
	else:
		plot_graph(graph, RES_DIR)

	print("Finished! Results in '" + RES_DIR + "'")


def create_graph(done_list):
	G = nx.DiGraph()
	border = {}
	border_id = len(done_list)
	for url in done_list:
		if "true_url" in done_list[url]:
			url = done_list[url]["true_url"]

		node_id = done_list[url]["id"]
		if node_id not in G:
			G.add_node(node_id, label=url)

		for outlink in done_list[url]["outlinks"]:
			outlink_id = border_id
			if outlink not in done_list:
				if outlink not in border:
					border[outlink] = border_id
					border_id += 1
				else:
					outlink_id = border[outlink]
			else:
				outlink_id = done_list[outlink]["id"]

			if outlink_id not in G:
				G.add_node(outlink_id, label=outlink)

			G.add_edge(node_id, outlink_id)
			
	return G



def compare_stats(graph, seed_count, RES_DIR):
	seed_nodes = [x for x in range(seed_count) if x in graph.nodes]
	
	pagerank_list = simulate_crawl(graph, seed_nodes, nx.pagerank(graph))
	hits_list = simulate_crawl(graph, seed_nodes, nx.hits(graph, tol=1.0e-3)[1])
	normal_list = simulate_crawl(graph, seed_nodes)

	# List of nodes sorted by indegree
	indegree_nodes = list(graph.in_degree())
	indegree_nodes.sort(key=lambda x: x[1], reverse=True)
	indegree_list = [x[0] for x in indegree_nodes]
	number_of_nodes = len(indegree_list)
	
	# Overlap data
	labels = ["pagerank", "hits", "breadth"]
	markers = ["s", "x", "^"]
	colors = ["k", "b", "r"]
	lists_to_compare = [pagerank_list, hits_list, normal_list]
	overlap_values_list = [[] for l in range(len(lists_to_compare))]
	# Check percentage of pages crawled, out of pages crawled if they were sorted by in degree
	# Example:
	# pagerank_list	| indegree_list	| percentage
	# 1				| 4				| 0%
	# 4				| 3				| 50% (crawled 4, 1/2)
	# 3				| 5				| 66% (crawled 3, 2/3)
	# 2				| 6				| 50% (2 is not in indegree_list yet, 2/4)
	# 5				| 1				| 80% (crawled 5, 1 appeared in indegree_list, 4/5)
	# 6				| 2				| 100% (crawling always ends at 100%, since all pages were crawled)
	for i in range(1, number_of_nodes+1):
		for overlap_values in overlap_values_list:
			overlap_values.append(0)
		for node in indegree_list[:i]:
			for list_index, comparing_list in enumerate(lists_to_compare):
				if node in comparing_list[:i]:
					overlap_values_list[list_index][-1] += 1
		for overlap_values in overlap_values_list:
			overlap_values[-1] /= i
	

	plot_overlap(overlap_values_list, labels, colors, markers, RES_DIR)



def simulate_crawl(graph, seed_nodes, order=None):
	""" Simulate crawling, prioritizing pages by order, where order is a dict of [node: auth]
	If no order is given, execute depth-first crawling
	"""
	crawled_list = []
	todo_list = []
	if order:
		for node in seed_nodes:
			insert_sorted(todo_list, node, order)
	else:
		todo_list = list(seed_nodes)
	done_list = {node: 1 for node in seed_nodes}
	while todo_list:
		# Pop last item (list in descending order)
		current_node = todo_list.pop(0)
		crawled_list.append(current_node)
		for neighbor in graph.neighbors(current_node):
			if neighbor not in done_list:
				done_list[neighbor] = 1
				if order:
					# Insert sorted descending
					insert_sorted(todo_list, neighbor, order)
				else:
					todo_list.insert(0, neighbor)
								
	return crawled_list


def plot_graph(G, RES_DIR):
	DOT_FILENAME = os.path.join(RES_DIR, "graph.dot")
	PNG_FILENAME = os.path.join(RES_DIR, "graph.svg")
	
	nx.drawing.nx_agraph.write_dot(G, DOT_FILENAME)
	subprocess.run('fdp -Tsvg "' + DOT_FILENAME + '" -o "'
			+ PNG_FILENAME + '"', shell = True, stderr = subprocess.DEVNULL)



def plot_overlap(overlap_values_list, labels, colors, markers, RES_DIR):
	number_of_nodes = len(overlap_values_list[0])
	for index, overlap_values in enumerate(overlap_values_list):
		plt.plot(overlap_values, label=labels[index], color=colors[index], marker=markers[index], markevery=number_of_nodes//20)
	
	ax = plt.gca()
	xaxis = ax.get_xticks()
	ax.set_xticklabels(["{}%".format(int(x*100/number_of_nodes)) for x in xaxis])
	yaxis = ax.get_yticks()
	ax.set_yticklabels(["{}%".format(int(y*100)) for y in yaxis])

	plt.title("Evolución del porcentaje de overlap\n respecto del orden por valor de Auth\n para Pagerank y HITS")
	plt.xlabel("x% crawled")
	plt.ylabel("Overlap with best x% by indegree")
	plt.legend()
	plt.savefig(os.path.join(RES_DIR, 'overlap.png'))



def insert_sorted(sorted_list, item, order):
	""" Inserts item into a sorted list, sorting in descending order by value of order[item] """
	curr_pos = len(sorted_list)
	sorted_list.append(item)
	while curr_pos > 0 and order[sorted_list[curr_pos -1]] < order[sorted_list[curr_pos]]:
		aux = sorted_list[curr_pos -1]
		sorted_list[curr_pos -1] = sorted_list[curr_pos]
		sorted_list[curr_pos] = aux
		curr_pos -= 1
	return curr_pos



if __name__ == "__main__":
	parser = argparse.ArgumentParser(description = "Simple HTTP Crawler made with Python.")
	parser.add_argument("seed", help = "Seed file with starting URL list to crawl")
	parser.add_argument("-m", "--max-crawl", help = "Max amount of pages to crawl (default " + str(MAX_CRAWL_DEFAULT) + ")", type = int, default = MAX_CRAWL_DEFAULT)
	parser.add_argument("-d", "--depth", help = "Max depth to crawl (default " + str(MAX_DEPTH_DEFAULT) + ")", type = int, default = MAX_DEPTH_DEFAULT)
	parser.add_argument("-s", "--stats", help = "Compare crawling by Page Rank, HITS and random", action="store_true")
	args = parser.parse_args()
	main(args.seed, args.max_crawl, args.depth, args.stats)