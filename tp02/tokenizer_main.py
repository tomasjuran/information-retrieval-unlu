#!/usr/bin/env python3

"""Main program to tokenize all documents in a directory"""

from os import makedirs
from os.path import basename, join
import argparse
import csv
from collections import OrderedDict

from nltk import SnowballStemmer, PorterStemmer, LancasterStemmer

from utilities.file_lister import file_lister
from utilities.file_to_string import file_to_string
from tokenizer.tokenizer import tokenizar, sacar_palabras_vacias, get_url, get_numero, get_nombre_propio, get_abbr, get_stopwords

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

SEPARATOR = "\t" # For output files and default separator in general

SNOWBALL = "snowball"
PORTER = "porter"
LANCASTER = "lancaster"

STOPWORD_FILENAME = "../aux-files/stop-word-list-es.txt"
RESULTS_DIR = "results/tokenizer"
TERMS_FILENAME = join(RESULTS_DIR, "terminos.txt")
STATS_FILENAME = join(RESULTS_DIR, "estadisticas.txt")
FREQS_FILENAME = join(RESULTS_DIR, "frecuencias.txt")

URLS_FILENAME = join(RESULTS_DIR, "urls.txt")
NUMS_FILENAME = join(RESULTS_DIR, "numbers.txt")
NAME_FILENAME = join(RESULTS_DIR, "names.txt")
ABBR_FILENAME = join(RESULTS_DIR, "abbr.txt")

def write_terms_file(vocabulary):
	with open(TERMS_FILENAME, "w") as file:
		file.write("term" + SEPARATOR + "CF" + SEPARATOR + "DF\n")
		for term, freqs in vocabulary.items():
			file.write(term  + SEPARATOR + str(freqs["CF"]) + SEPARATOR + str(freqs["DF"]) + "\n")

def write_stats_file(vocabulary, documents_q, tokens_q, small_doc_tokens, small_doc_terms, large_doc_tokens, large_doc_terms):
	terms_q = len(vocabulary)
	mean_tokens_q = tokens_q / documents_q
	mean_terms_q = terms_q / documents_q
	sum_term_len = 0
	unique_terms = 0
	for term in vocabulary.keys():
		sum_term_len += len(term)
		if vocabulary[term]["CF"] == 1:
			unique_terms += 1
	mean_term_len = sum_term_len / terms_q

	with open(STATS_FILENAME, "w") as file:
		file.write(str(documents_q) + "\n")
		file.write(str(tokens_q) + SEPARATOR + str(terms_q) + "\n")
		file.write(str(mean_tokens_q) + SEPARATOR + str(mean_terms_q) + "\n")
		file.write(str(mean_term_len) + "\n")
		file.write(str(small_doc_tokens) + SEPARATOR + str(small_doc_terms) + SEPARATOR + str(large_doc_tokens) + SEPARATOR + str(large_doc_terms) + "\n")
		file.write(str(unique_terms))

def write_freqs_file(vocabulary):
	items = list(vocabulary.items())
	with open(FREQS_FILENAME, "w") as file:
		for i in range(10):
			file.write(items[i][0] + SEPARATOR + str(items[i][1]["CF"]) + "\n")
		for i in range(len(items)-10, len(items)):
			file.write(items[i][0] + SEPARATOR + str(items[i][1]["CF"]) + "\n")

def write_extract_files(urls_list, nums_list, name_list, abbr_list):
	write_file(URLS_FILENAME, urls_list)
	write_file(NUMS_FILENAME, nums_list)
	write_file(NAME_FILENAME, name_list)
	write_file(ABBR_FILENAME, abbr_list)

def write_file(filename, token_list):
	with open(filename, "w") as file:
		for token in token_list:
			file.write(token + "\n")

def main(dirname, encoding=None, stopword_filename=None, separator=SEPARATOR, no_extract=True, stemming=None):
	lista_vacias = get_stopwords(stopword_filename, separator)
	vocabulary = {}
	documents_q = 0
	tokens_q = 0
	small_doc_tokens = None
	small_doc_terms = 0
	large_doc_tokens = None
	large_doc_terms = 0

	if not no_extract:
		urls_list = []
		nums_list = []
		name_list = []
		abbr_list = []

	for document in file_lister(dirname):
		documents_q += 1
		string = file_to_string(document)
		
		# Extract abbreviations, urls, numbers and proper nouns
		if not no_extract:
			(string, tokens) = get_url(string)
			urls_list.extend(tokens)
			(string, tokens) = get_numero(string)
			nums_list.extend(tokens)
			(string, tokens) = get_nombre_propio(string)
			name_list.extend(tokens)
			(string, tokens) = get_abbr(string)
			abbr_list.extend(tokens)

		lista_tokens = tokenizar(string)
		tokens_q += len(lista_tokens)

		if stemming is not None:
			stemming = stemming.lower()
			if stemming == PORTER:
				stemming_func = PorterStemmer().stem
			elif stemming == LANCASTER:
				stemming_func = LancasterStemmer().stem
			else:
				stemming_func = SnowballStemmer("spanish").stem
			for index, token in enumerate(lista_tokens):
				lista_tokens[index] = stemming_func(token)
		
		# Add terms to vocabulary
		lista_terminos = sacar_palabras_vacias(lista_tokens, lista_vacias)
		for term, freq in lista_terminos.items():
			if term in vocabulary:
				vocabulary[term]["CF"] += freq
				vocabulary[term]["DF"] += 1
			else:
				vocabulary[term] = {"CF":freq, "DF":1}

		# Check if shortest document
		if not small_doc_tokens or len(lista_tokens) < small_doc_tokens:
			small_doc_tokens = len(lista_tokens)
			small_doc_terms = len(lista_terminos)

		# Check if largest document
		if not large_doc_tokens or len(lista_tokens) > large_doc_tokens:
			large_doc_tokens = len(lista_tokens)
			large_doc_terms = len(lista_terminos)

		print("Document " + str(documents_q) + " (" + basename(document) + "): "\
		 + str(len(lista_tokens)) + " tokens, " + str(len(lista_terminos)) + " terms")

	ord_vocabulary = OrderedDict(sorted(vocabulary.items(), key=lambda x: x[1]["CF"], reverse=True))
	makedirs(RESULTS_DIR, exist_ok=True)
	write_terms_file(ord_vocabulary)
	write_stats_file(ord_vocabulary, documents_q, tokens_q, small_doc_tokens, small_doc_terms,\
	 large_doc_tokens, large_doc_terms)
	write_freqs_file(ord_vocabulary)
	if not no_extract:
		write_extract_files(urls_list, nums_list, name_list, abbr_list)

	print("Results saved in directory \"" + RESULTS_DIR + "\"")

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Tokenize documents.")
	parser.add_argument("dirname", help="Directory with documents to tokenize")
	parser.add_argument("-e", "--encoding", help="Encoding to use (if not specified, try to guess encoding with chardet module)")
	parser.add_argument("-s", "--stopwords", help="Stopwords file to use")
	parser.add_argument("--separator", help="Separator for words in stopwords file")
	parser.add_argument("--no-extract", action="store_true", help="Don't extract abbreviations, urls, numbers or proper nouns")
	parser.add_argument("--stemming", help="Use stemming to tokenize (Snowball for spanish, Porter and Lancaster for english)")
	args = parser.parse_args()
	main(args.dirname, args.encoding, args.stopwords, args.separator, args.no_extract, args.stemming)