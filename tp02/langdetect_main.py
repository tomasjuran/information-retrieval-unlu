#!/usr/bin/env python3

"""Main program to execute langdetect."""

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

from os import makedirs
from os.path import basename, join
import argparse

from langdetect import detect, detect_langs
import re

from utilities.file_lister import file_lister
from utilities.file_to_string import file_to_string
from languagedetect.languagedetect import langdetect

SEPARATOR = "\t"

SOLUTION_FILENAME = "solution"
TESTING_FILENAME = "test"
RESULTS_DIR = join("results", "langdetect")

def pypi_langdetect(dirname):
	solution = []
	with open(join(dirname, SOLUTION_FILENAME), "r") as file:
		for line in file:
			solution.append(re.findall(r"[a-zA-Z]+", line)[0])

	lines_detected = []
	lines_read = 0
	matches = 0
	with open(join(dirname, TESTING_FILENAME), "r", errors="ignore") as file:
		for line in file:
			lines_read += 1
			detected = detect(line)
			if detected == "en":
				detected = "English"
			elif detected == "it":
				detected = "Italian"
			elif detected == "fr":
				detected = "French"
			lines_detected.append([str(lines_read), detected, str(detect_langs(line)[0].prob), solution[lines_read-1]])
			if detected == solution[lines_read-1]:
				matches += 1

	results_filename = join(RESULTS_DIR, "PyPI-langdetect")
	with open(results_filename, "w") as file:
		file.write("Line" + SEPARATOR + "Detected" + SEPARATOR + "Probability" + SEPARATOR + "Language\n")
		for line in lines_detected:
			file.write(SEPARATOR.join(line) + "\n")

	print("Detected language correctly on " + str(matches) + "/" + str(lines_read) + \
			" (" + "{0:.1f}".format((matches/lines_read)*100) + "%) lines. Results saved in \"" + results_filename + "\"")


def main(dirname, method, force_training):
	languages = file_lister(join(dirname, "training"))
	languages = [basename(language) for language in languages]
	langdetect(dirname, languages, method, force_training)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Detect language.")
	parser.add_argument("dirname", help="Directory with documents to train and test")
	parser.add_argument("--method", "-m", help="Method to use (unigram or bigram)")
	parser.add_argument("--force-training", "-f", action="store_true", help="Force retraining of the model")
	parser.add_argument("--use-pypi-langdetect", "-p", action="store_true", help="Use PyPI's langdetect module")
	args = parser.parse_args()
	if (args.use_pypi_langdetect):
		pypi_langdetect(args.dirname)
	else:
		main(args.dirname, args.method, args.force_training)