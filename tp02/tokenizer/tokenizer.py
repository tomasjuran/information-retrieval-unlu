"""This module provides functions for lexically analyzing a document
and returning its logical representation as a list of terms."""

import re

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

TILDE_UPPER = "ÁÀÉÈÍÌÓÒÚÙÜÑ"
TILDE_LOWER = "áàéèíìóòúùüñ"
SPECIAL_CHARS = TILDE_LOWER + TILDE_UPPER
ALPHABET = "a-zA-Z" + SPECIAL_CHARS
MIN_TERM_SIZE = 3
MAX_TERM_SIZE = 20

def get_url(string, cut=True):
	"""Obtain urls and mail addresses. Returns the string and a list of tokens

	Keyword arguments:
	string -- the string to process
	cut -- if True, replace occurrences with a space
	"""
	patterns = []
	# Emails
	patterns.append(re.compile(r"\S+@[a-zA-Z]+\.[a-zA-Z]{1,3}(?:\.[a-zA-Z]{1,2})?"))
	# URLs
	patterns.append(re.compile(r"(?:[a-zA-Z]+://)?[a-zA-Z\-.]+\.[a-z]+(?::\d+)?(?:[/?#]\S+)*"))
	return apply_patterns(patterns, string, cut)

def get_numero(string, cut=True):
	"""Obtain certain format of numbers: money, percentages, dates, telephones. Returns the string and a list of tokens

	Keyword arguments:
	string -- the string to process
	cut -- if True, replace occurrences with a space
	"""
	patterns = []
	# Money
	patterns.append(re.compile(r"\$\d+(?:[,.]\d+)?"))
	# Percentages
	patterns.append(re.compile(r"\d+(?:[,.]\d+)?\%"))
	# Dates
	patterns.append(re.compile(r"\d{1,4}[-/]\d{1,2}[-/]\d{1,4}"))
	# Phones in formats according to ITU-T E.164
	# (415) 555-2671, (11) 15 1234-5678, (2966) 15 12-3456
	patterns.append(re.compile(r"\(\d{1,4}\)\s(?:\d{1,2}[\s-])?\d{1,4}[\s-]?\d{4}"))
	# +1 415 5552671, +44 20 7183 8750, +54 9 11 29455282
	patterns.append(re.compile(r"\+\d{1,2}\s(?:\d\s)?\d{1,3}\s\d{1,4}[\s-]?\d{4}"))
	# 020 7183 8750, 011 2839-5821, 02323 423979
	patterns.append(re.compile(r"\d{1,5}[\s-]\d{1,4}[\s-]?\d{4}"))
	# Quantities (ml. g. Ha.)
	patterns.append(re.compile(r"\d+(?:[,.]\d+)? ?[a-zA-Z]{1,2}\."))
	# Generic numbers
	# (?<![a-zA-Z])\d+(?:[.,]\d+)?(?![a-zA-Z])
	patterns.append(re.compile(r"(?<![" + ALPHABET + r"])\d+(?:[.,]\d+)?(?![" + ALPHABET + r"])"))
	return apply_patterns(patterns, string, cut)
	
def get_nombre_propio(string, cut=True):
	"""Obtain proper nouns. Returns the string and a list of tokens

	Keyword arguments:
	string -- the string to process
	cut -- if True, replace occurrences with a space
	"""
	# [A-Z][a-zA-Z]{2,}(?: [A-Z][a-zA-Z]{2,})+
	pattern = re.compile(r"[A-Z" + TILDE_UPPER + r"][" + ALPHABET + r"]{2,}" + \
		r"(?: [A-Z" + TILDE_UPPER + r"][" + ALPHABET + r"]{2,})+")
	return apply_patterns([pattern], string, cut)	

def get_abbr(string, cut=True):
	"""Obtain abbreviations. Returns the string and a list of tokens

	Keyword arguments:
	string -- the string to process
	cut -- if True, replace occurrences with a space
	"""
	modified_string = string
	# See "What is a word, what is a sentence? Problems of Tokenization"
	step1_pattern = re.compile(r"(?:[" + ALPHABET + r"]\.?){2,}\.(?= ?[,;a-z" + TILDE_LOWER + r"])")
	abbr_pattern = re.compile(r"(?:[" + ALPHABET + r"]\.?){2,}\.")

	tokens = step1_pattern.findall(string)
	modified_string = step1_pattern.sub(" ", string)

	step2 = abbr_pattern.findall(modified_string)
	step3 = []

	delimiter = "[^" + ALPHABET + "\\.]"
	for word in step2:
		# If word appears without the trailing period, it's not an abbreviation; don't include in step 3
		if re.search(delimiter + word[:-1] + delimiter, modified_string) is None:
			step3.append(word)

	for word in step3:
		# If word appears more than twice, it is an abbreviation
		if len(re.findall(delimiter + word[:-1] + r"\." + delimiter, modified_string)) > 2:
			modified_string = re.sub(delimiter + word[:-1] + r"\." + delimiter, " ", modified_string)
			tokens.append(word)

	if cut:
		string = modified_string
	return (string, tokens)

def apply_patterns(patterns, string, cut=True):
	tokens = []
	for pattern in patterns:
		tokens.extend(pattern.findall(string))
	if cut:
		for pattern in patterns:
			string = pattern.sub(" ", string)
	return (string, tokens)	

def tokenizar(string):
	"""Lexically analyzes the string and returns a list of tokens that represent it.

	Keyword arguments:
	string -- the string to process
	"""
	# (?:[^a-zA-Z])?([a-zA-Z]{3,20})(?:[^a-Z])?
	return re.findall(r"(?<![" + ALPHABET + r"])([" + ALPHABET + r"]{" + str(MIN_TERM_SIZE) + r"," + str(MAX_TERM_SIZE) + r"})(?![" + ALPHABET + r"])", string)

def sacar_palabras_vacias(lista_tokens, lista_vacias):
	"""Removes duplicates and words in lista_vacias from lista_tokens. Returns the normalized vocabulary.

	Keyword arguments:
	lista_tokens -- a list of tokens or words
	lista_vacias -- a list of stopwords
	"""
	lista_terminos = {}
	for token in lista_tokens:
		if not (token.lower() in lista_vacias):
			if token in lista_terminos:
				lista_terminos[token] += 1
			else:
				lista_terminos[token] = 1
	return lista_terminos

def get_stopwords(filename=None, separator="\t"):
	"""Obtains a list of stopwords from a file

	Keyword arguments:
	filename -- name of the stopwords file
	"""
	stopwords = {}
	if filename is not None:
		with open(filename, "r") as file:
			for line in file:
				stopwords = {**stopwords, **{stopword.rstrip():0 for stopword in line.split(separator)}}
	return stopwords