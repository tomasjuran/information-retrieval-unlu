"""This module takes a file and returns its contents in a single string."""

from os import path

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

def file_to_string(filename, encoding=None):
	"""Take a file and returns its contents in a single string.

	Keyword arguments:
	filename -- path of the file
	encoding -- encoding to use
	"""
	string = ""
	if path.isfile(filename):
		
		if encoding is None:
			import chardet
			rawdata = open(filename, "rb").read()
			result = chardet.detect(rawdata)
			encoding = result["encoding"]

		with open(filename, "r", encoding=encoding) as file:
			for line in file:
				string += line
	return string