"""This module receives a directory list and creates each directory if it does not already exist."""

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

from os import makedirs
from os.path import isdir

def create_dirs(dir_list):
    for dir in dir_list:
        if not isdir(dir):
            makedirs(dir)