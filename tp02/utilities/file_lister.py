"""This module receives a directory path and lists the files inside it.
	Can also search recursively.
"""

from os import listdir
from os.path import isdir, isfile, join

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

def file_lister(dirname, recursive=True):
	"""List files inside a directory.
	
	Keyword arguments:
	dirname -- directory path
	recursive -- if True, search for files in sub-directories. Default True
	"""
	file_list = []
	if isdir(dirname):
		for path in listdir(dirname):
			fullpath = join(dirname, path)
			if isfile(fullpath):
				file_list.append(fullpath)
			elif recursive and isdir(fullpath):
				file_list.extend(file_lister(fullpath, recursive))

	return file_list