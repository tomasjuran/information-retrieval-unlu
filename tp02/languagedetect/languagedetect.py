"""This module provides functions for detecting the language of each line in a file."""

from os import makedirs
from os.path import join, isdir, isfile
import re
import csv

from collections import OrderedDict
import numpy as np

from utilities.file_to_string import file_to_string

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

SEPARATOR = "\t"

UNIGRAM = "unigram"
BIGRAM = "bigram"

LANGUAGES = ["English", "French", "Italian"]

LANG_ID_DIR = "languageIdentificationData"
SOLUTION_FILENAME = "solution"
TESTING_FILENAME = "test"
TRAINING_DIR = "training"

RESULTS_DIR = join("results", "langdetect")
TRAINING_RESULTS_DIR = join(RESULTS_DIR, "training")

def read_training_file(dirname, language):
	training_filename = join(dirname, TRAINING_DIR, language)
	return file_to_string(training_filename).lower()

def read_solution_file(dirname):
	solution = []
	with open(join(dirname, SOLUTION_FILENAME), "r") as file:
		for line in file:
			solution.append(re.findall(r"[a-zA-Z]+", line)[0])
	return solution

def distribution_to_frequencies(distribution):
	freq_list = []
	for freq in distribution.values():
		# Ordered frequencies
		freq_list.append(float(freq))
	return freq_list

def train_unigram(dirname, languages):
	char_distribution = {}
	for language in languages:
		training_file = read_training_file(dirname, language)
		char_distribution[language] = get_unigram_distribution(training_file)
		training_result_filename = join(TRAINING_RESULTS_DIR, "uni_" + language)
		with open(training_result_filename, "w") as file:
			writer = csv.DictWriter(file, char_distribution[language].keys())
			writer.writeheader()
			writer.writerow(char_distribution[language])
		print("Training file for " + language + " saved in \"" + training_result_filename + "\"")

	return char_distribution

def initialize_unigram_distribution():
	distribution = {}
	for char in list(map(chr, range(ord('a'), ord('z')+1))):
		distribution[char] = 0
	return distribution

def get_unigram_distribution(string):
	distribution = initialize_unigram_distribution()
	total_count = 0
	for char in string:
		char = char.lower()
		if re.match(r"[a-z]", char) is not None:
			distribution[char] += 1
			total_count += 1
	distribution = {key:freq / total_count for key, freq in distribution.items()}
	distribution = OrderedDict(sorted(distribution.items()))
	return distribution

def get_training_distribution_unigram(languages):
	training_distribution = {}
	for language in languages:
		with open(join(TRAINING_RESULTS_DIR, "uni_" + language), "r") as file:
			reader = csv.DictReader(file)
			distribution = OrderedDict(sorted(next(reader).items()))
			training_distribution[language] = distribution_to_frequencies(distribution)	
	return training_distribution

def train_bigram(dirname, languages):
	bigram_distribution = {}
	for language in languages:
		training_file = read_training_file(dirname, language)
		bigram_distribution[language] = get_bigram_distribution(training_file)
		training_result_filename = join(TRAINING_RESULTS_DIR, "bi_" + language)
		with open(training_result_filename, "w") as file:
			writer = csv.DictWriter(file, bigram_distribution[language].keys())
			writer.writeheader()
			writer.writerow(bigram_distribution[language])
		print("Training file for " + language + " saved in \"" + training_result_filename + "\"")

	return bigram_distribution 

def initialize_bigram_distribution():
	distribution = {}
	for preceding in list(map(chr, range(ord('a'), ord('z')+1))):
		for following in list(map(chr, range(ord('a'), ord('z')+1))):
			distribution[preceding + following] = 0
	return distribution

def get_bigram_distribution(string):
	distribution = initialize_bigram_distribution()
	total_count = 0
	for index in range(1,len(string)):
		bigram = string[index-1] + string[index]
		if re.match(r"[a-z]{2}", bigram) is not None:
			distribution[bigram] += 1
			total_count += 1
	distribution = {key:freq / total_count for key, freq in distribution.items()}
	distribution = OrderedDict(sorted(distribution.items()))
	return distribution

def get_training_distribution_bigram(languages):
	training_distribution = {}
	for language in languages:
		training_distribution[language] = []
		with open(join(TRAINING_RESULTS_DIR, "bi_" + language), "r") as file:
			reader = csv.DictReader(file)
			for row in reader:
				training_distribution[language].append(distribution_to_frequencies(row))
	return training_distribution

def langdetect(dirname=LANG_ID_DIR, languages=LANGUAGES, method=UNIGRAM, force_recalc=False):
	"""Detect the language of each line in a file

	Keyword arguments:
	dirname -- directory where the language identification data is located. Has following structure:
		dirname/
			solution_file
			testing_file
			training/
				language1
				language2
				language3
	languages -- languages to process
	method -- method to use
	force_recalc -- force overwrite of previous training files
	"""
	method = str(method).lower()
	if method not in (UNIGRAM, BIGRAM):
		print("Method \"" + str(method) + "\" not available. Proceeding with " + UNIGRAM)
		method = UNIGRAM

	if force_recalc or not isdir(TRAINING_RESULTS_DIR):
		makedirs(TRAINING_RESULTS_DIR, exist_ok=True)
		if method == UNIGRAM:
			train_unigram(dirname, languages)
		else:
			train_bigram(dirname, languages)

	if method == UNIGRAM:
		training_distribution = get_training_distribution_unigram(languages)
	else:
		training_distribution = get_training_distribution_bigram(languages)

	solution = read_solution_file(dirname)
	testing_filename = join(dirname, TESTING_FILENAME)
	results_filename = join(RESULTS_DIR, method)

	with open(testing_filename, "r", errors="ignore") as testing_file, \
			open(results_filename, "w") as results_file:
		
		results_file.write("Line" + SEPARATOR + "Detected" + SEPARATOR + "Correlation" + SEPARATOR + "Language\n")
		lines_read = 0
		matches = 0
		for line in testing_file:
			line = line.lower()
			lines_read += 1
			
			frequencies = []
			if method == UNIGRAM:
				distribution = get_unigram_distribution(line)
			else:
				distribution = get_bigram_distribution(line)
			frequencies = distribution_to_frequencies(distribution)
			
			max_correlation = 0
			detected_language = languages[0]
			for language in languages:
				correlation_matrix = np.corrcoef(training_distribution[language], frequencies)
				correlation = correlation_matrix[0][1]
				if correlation > max_correlation:
					detected_language = language
					max_correlation = correlation

			if detected_language == solution[lines_read-1]:
				matches += 1

			results_file.write(str(lines_read) + SEPARATOR + detected_language \
					+ SEPARATOR + str(max_correlation) + SEPARATOR + solution[lines_read-1] + "\n")

		print("Detected language correctly on " + str(matches) + "/" + str(lines_read) + \
				" (" + "{0:.1f}".format((matches/lines_read)*100) + "%) lines. Results saved in \"" + results_filename + "\"")